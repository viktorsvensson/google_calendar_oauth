package com.example.spring_boot_google_calendar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootGoogleCalendarApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootGoogleCalendarApplication.class, args);
    }

}
