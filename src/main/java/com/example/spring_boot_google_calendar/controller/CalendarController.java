package com.example.spring_boot_google_calendar.controller;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Events;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.GeneralSecurityException;

@RestController
@RequestMapping("/api/calendar")
public class CalendarController {

    @GetMapping
    public Events getCalendarEvents(
            @RegisteredOAuth2AuthorizedClient("google")OAuth2AuthorizedClient client
            ) throws GeneralSecurityException, IOException {

        String accessToken = client.getAccessToken().getTokenValue();
        GoogleCredential googleCredential = new GoogleCredential()
                .setAccessToken(accessToken);
        NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        Calendar calendar = new Calendar
                .Builder(httpTransport, new GsonFactory(), googleCredential)
                .build();

        return calendar.events().list("primary").execute();


    }

}
